import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {
  protected aFormGroup: FormGroup;
  isCaptchaValid: boolean = false;
  captchaResolved: boolean = false;
  siteKey: string = "6LeKjWgpAAAAAH4vmoMRUbI7DQNPKaQS2JuMrZsG";
  emp: any;

  //Dependency Injection for EmpService, Router
  constructor(private router: Router, private service: EmpService,private formBuilder: FormBuilder,) { 
    this.aFormGroup = this.formBuilder.group({
      recaptcha: ['', Validators.required]
    });  
  }

  ngOnInit(){
  }
  onCaptchaResolved(event: any) {
    // Handle captcha resolved event
    this.isCaptchaValid = true;
    this.captchaResolved = true;
  }

  async loginSubmit(loginForm: any) {
    if (loginForm.emailId == 'HR' && loginForm.password == 'HR') {           
      //Setting the isUserLoggedIn variable value to true under EmpService
      this.service.setIsUserLoggedIn();
      localStorage.setItem("emailId", loginForm.emailId);
      this.router.navigate(['showemps']);
    } else {
      this.emp = null;

      await this.service.employeeLogin(loginForm.emailId, loginForm.password).then((data: any) => {
        console.log(data);
        this.emp = data;
      });

      if (this.emp != null) {
        this.service.setIsUserLoggedIn();        
        localStorage.setItem("emailId", loginForm.emailId);
        this.router.navigate(['products']);
      } else {
        alert('Invalid Credentials');
      }
    }
  }
}
