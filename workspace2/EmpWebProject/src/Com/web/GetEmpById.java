package Com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Com.dao.EmployeeDAO;
import Com.dto.Employee;

/**
 * Servlet implementation class GetEmpById
 */
@WebServlet("/GetEmpById")
public class GetEmpById extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetEmpById() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();

			int empId = Integer.parseInt(request.getParameter("empId"));
			
			EmployeeDAO empDao = new EmployeeDAO();
			Employee emp = empDao.getEmployeeById(empId);
			
			if (emp != null) {
				
				//Storing the employee data in request object
				request.setAttribute("emp", emp);
				
				RequestDispatcher rd = request.getRequestDispatcher("GetEmployeeById.jsp");
				rd.forward(request, response);
							
			} else {
				RequestDispatcher rd = request.getRequestDispatcher("HRHomePage.jsp");
				rd.include(request, response);
				
				out.println("<center>");
				out.println("<h1 style='color:red;'>Unable to Fetch Employee Record!!!</h1>");
				out.println("</center>");
			}
		}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
