import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { authGuard } from './auth.guard';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ShowEmployeeComponent } from './show-employee/show-employee.component';
import { ShowempbyidComponent } from './showempbyid/showempbyid.component';
import { ProductsComponent } from './products/products.component';
import { LogoutComponent } from './logout/logout.component';
import { CartComponent } from './cart/cart.component';


const routes: Routes = [
  {path:'', component:LoginComponent},
  {path:'login', component:LoginComponent},
  {path:'register', component:RegisterComponent},
  {path:'showemps', canActivate:[authGuard], component:ShowEmployeeComponent},
  {path:'showempbyid',canActivate:[authGuard], component:ShowempbyidComponent},
  {path:'products', canActivate:[authGuard],component:ProductsComponent},
  {path:'logout', canActivate:[authGuard],component:LogoutComponent},
  {path:'cart',canActivate:[authGuard], component:CartComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
