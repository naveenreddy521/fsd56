package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Customer;

@Service
public class CustomerDao {
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	private JavaMailSender mailSender;

	public List<Customer> getAllCustomers() {
		return customerRepository.findAll();
	}

	public Customer addCustomer(Customer customer) {
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        String encryptedPwd = bcrypt.encode(customer.getPassword());
        customer.setPassword(encryptedPwd);

        Customer savedCustomer = customerRepository.save(customer);
        
		sendWelcomeEmail(savedCustomer);

        return savedCustomer;

	}
	 private void sendWelcomeEmail(Customer customer) {
			
			SimpleMailMessage message = new SimpleMailMessage();
			message.setTo(customer.getEmailId());
			message.setSubject("Welcome to our website");
			message.setText("Dear " + customer.getCustomerName() + ",\n\n"
					+ "Thank you for registering ");

			mailSender.send(message);
		}

	public Customer customerLogin(String emailId, String password) {
		return customerRepository.customerLogin(emailId,password);
	}
}
