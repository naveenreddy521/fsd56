package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Course;

@Service
public class CourseDao {
	
	// Implementing Dependency Injection for CourseRepository
	@Autowired
	private CourseRepository courseRepository;

	public List<Course> getAllCourses() {
		return courseRepository.findAll();
	}

	public Course getCourseById(int courseId) {
		Course courseNotFound = new Course(0, "Course Not Found!!!", 0.0);
		return courseRepository.findById(courseId).orElse(courseNotFound);
	}

	public List<Course> getCourseByName(String courseName) {
		return courseRepository.findByName(courseName);
	}

	public Course addCourse(Course course) {
		return courseRepository.save(course);
	}

	public Course deleteCourseById(int courseId){
		courseRepository.deleteById(courseId);
		return null;
	}

	public Course updateCourse(Course updatedCourse) {
		int courseId = updatedCourse.getCourseId();

		if (courseRepository.existsById(courseId)) {
			return courseRepository.save(updatedCourse);
		} else {
			return null;
		}
	}

	
}