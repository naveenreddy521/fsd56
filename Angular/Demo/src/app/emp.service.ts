import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  isUserLoggedIn: boolean;
  loginStatus: any;
  cartItems: any;

  constructor(private http: HttpClient) { 
    this.cartItems = [];
    this.isUserLoggedIn = false;
    this.loginStatus = new Subject();
  }
  getAllCountries(){
    return this.http.get('https://restcountries.com/v3.1/all');
  }
  getAllEmployees():any{
    return this.http.get('http://localhost:8085/getEmployees')
  }
  getEmployeeById(empId:any):any{
    return this.http.get('http://localhost:8085/getEmployeeById/' + empId);
  }
regsiterEmployee(employee: any): any {
  return this.http.post('http://localhost:8085/addEmployee', employee);
  }
getAllDepartments(): any {
  return this.http.get('http://localhost:8085/getDepartments');
  }
deleteEmployee(empId: any):any{
  return this.http.delete('http://localhost:8085/deleteEmployeeById/'+empId);
  }
employeeLogin(emailId: any, password: any): any {
  return this.http.get('http://localhost:8085/empLogin/' + emailId + '/' + password).toPromise();
}
updateEmployee(employee: any) {
  return this.http.put('http://localhost:8085/updateEmployee', employee);
}



  //Login
  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }
  getIsUserLogged(): boolean {
    return this.isUserLoggedIn;
  }  
  getLoginStatus(): any {
    return this.loginStatus.asObservable();
  }
  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
    this.loginStatus.next(false);
  }
  addToCart(product: any) {
    this.cartItems.push(product);
  }
  getCartItems() {
    return this.cartItems;
  }
  setCartItems() {
    this.cartItems.splice();
  }

}