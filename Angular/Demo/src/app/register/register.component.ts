import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EmpService } from '../emp.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  countries: any;
  departments: any;
  emp: any;
  confirmPassword: string = ''; // Added variable for confirmPassword
  protected aFormGroup: FormGroup;
  isCaptchaValid: boolean = false;
  captchaResolved: boolean = false;
  siteKey: string = "6LeJjWgpAAAAALvszYULjRzlg7eUUo_49Q_wsHnO";

  constructor(private service: EmpService, private router: Router,private formBuilder: FormBuilder) {
    this.emp = {
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: '',
      department: {
        deptId: ''
      }
    };
    this.aFormGroup = this.formBuilder.group({
      recaptcha: ['', Validators.required]
    });
  }
  onCaptchaResolved(event: any) {
    // Handle captcha resolved event
    this.isCaptchaValid = true;
    this.captchaResolved = true;
  }

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
    this.service.getAllDepartments().subscribe((data: any) => { this.departments = data; });
  }
  registerSubmit(regForm: any) {
    if (this.emp.password !== this.confirmPassword) {
      console.log('Password and Confirm Password must be the same.');
  
    }

    this.emp.empName = regForm.empName;
    this.emp.salary = regForm.salary;
    this.emp.gender = regForm.gender;
    this.emp.doj = regForm.doj;
    this.emp.country = regForm.country;
    this.emp.emailId = regForm.emailId;
    this.emp.password = regForm.password;
    this.emp.department.deptId = regForm.department;

    console.log(this.emp);

    this.service. regsiterEmployee(this.emp).subscribe((data: any) => { console.log(data); });

    this.router.navigate(['login']);
  }
}