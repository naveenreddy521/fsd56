package com.ts;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CourseDao;
import com.model.Course;;


@RestController
public class CourseController {
	@Autowired
	CourseDao courseDao;
	
	//HardCoded
	@GetMapping("getCourse")
	public Course getCourse() {
		Course course= new Course();
		course.setCourseId(1);
	course.setCourseName("Java FullStack");
	course.setFee(60000.00);
		
		return course;
	}
	
	@GetMapping("getCourses")
	public List<Course> getCourses() {
		
		Course course1 = new Course(1, "JavaFullStack",60000.00);
		Course course2 = new Course(2, "PythonFullStack",35000.00);
		Course course3 = new Course(3, "AWS", 50000.00);
		
		List<Course> courseList = new ArrayList<Course>();
		courseList.add(course1 );
		courseList.add(course2);
		courseList.add(course3);
		
		return courseList;
	}
	@GetMapping("getAllCourses")
	public List<Course> getAllCourses() {		
		return courseDao.getAllCourses();
	}


	@GetMapping("getCourseById/{courseId}")
	public Course getCourseById(@PathVariable("courseId") int courseId) {
		return courseDao.getCourseById(courseId);	
	}
	
	@GetMapping("getCourseByName/{courseName}")
	public List<Course> getCourseByName(@PathVariable("courseName") String courseName) {
		return courseDao.getCourseByName(courseName);
	}
	@PostMapping("addCourse")
	public Course addCourse(@RequestBody Course course) {
		return courseDao.addCourse(course);
	}
	@DeleteMapping("deleteCourseById/{courseId}")
	public Course deleteCourseById(@PathVariable int courseId){
	    return courseDao.deleteCourseById(courseId);

	
}}