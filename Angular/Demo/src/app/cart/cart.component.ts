import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent implements OnInit {
  localStorageData: any;
  emailId: any;
  total: number;
  cartItems: any;
  //Cart using Service
  products: any;

  constructor(private service: EmpService) {
    this.cartItems = service.getCartItems();
    this.total = 0;
    this.emailId = localStorage.getItem('emailId');

    this.products = service.getCartItems();

    this.products.forEach((element: any) => {
      this.total = this.total + parseInt(element.price);
    });

    // this.localStorageData = localStorage.getItem('cartItems');
    // this.products = JSON.parse(this.localStorageData);
    // console.log(this.products);
  }

  ngOnInit() {
  }

  deleteCartProduct(product: any) {
    const i = this.products.findIndex((element: any) => {
      return element.id == product.id;
    });
    this.products.splice(i, 1);
    this.total = this.total - product.price;
  }

  purchase() {
    this.products = [];
    this.total = 0;
    this.service.setCartItems();
  }

}