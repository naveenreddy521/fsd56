package com.dao;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Employee;
import com.twilio.Twilio;
import com.twilio.exception.ApiException;
import com.twilio.rest.api.v2010.account.Message;

@Service
public class EmployeeDao {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	private JavaMailSender mailSender;
	
	 private static final int OTP_LENGTH = 6;
	
	 private static final String ACCOUNT_SID = "AC2f4bcb960b99e9126a5337740afc3525";
	    private static final String AUTH_TOKEN = "a41f7e4d07d6bc4a5ee9c7c2329e46b4";
	    private static final String TWILIO_PHONE_NUMBER = "+16592177740";
	    
	    static {
	        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
	    }

	public List<Employee> getEmployees() {
		return employeeRepository.findAll();
	}

	public Employee getEmployeeById(int employeeId) {
		return employeeRepository.findById(employeeId).orElse(null);
	}

	public Employee getEmployeeByName(String employeeName) {
		return employeeRepository.findByName(employeeName);
	}

	public Employee employeeLogin(String emailId, String password) {
        Employee employee = employeeRepository.employeeLogin(emailId, password);

        return employee;
    }

   

	 private String generateOTP() {
	        Random random = new Random();
	        StringBuilder otp = new StringBuilder();

	        for (int i = 0; i < OTP_LENGTH; i++) {
	            otp.append(random.nextInt(10));
	        }

	        return otp.toString();
	    }

	    // Add employee with OTP generation
	    public Employee addEmployee(Employee employee) {
	        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
	        String encryptedPwd = bcrypt.encode(employee.getPassword());
	        employee.setPassword(encryptedPwd);

	        // Generate OTP
	        String otp = generateOTP();
	        employee.setOtp(otp);

	        // Save the employee
	        Employee savedEmployee = employeeRepository.save(employee);


	        // Send OTP via SMS using Twilio
	        sendOtpViaSms(savedEmployee);
	        sendWelcomeEmail(savedEmployee);


	        return savedEmployee;
	    }
	    
	    private void sendWelcomeEmail(Employee employee) {
			
			SimpleMailMessage message = new SimpleMailMessage();
			message.setTo(employee.getEmailId());
			message.setSubject("Welcome to our website");
			message.setText("Dear " + employee.getEmpName() + ",\n\n"
					+ "Thank you for registering ");

			mailSender.send(message);
		}

		// Send OTP via SMS using Twilio
	    private void sendOtpViaSms(Employee employee) {
	        try {
	            Message message = Message.creator(
	                    new com.twilio.type.PhoneNumber(employee.getPhNumber()),
	                    new com.twilio.type.PhoneNumber(TWILIO_PHONE_NUMBER),
	                    "Your OTP for registration is: " + employee.getOtp())
	                    .create();

	            System.out.println("OTP sent successfully via SMS.");
	        } catch (ApiException e) {
	            if (e.getCode() == 21614) {
	                // Twilio error code 21614 corresponds to "Trial accounts cannot send messages to unverified numbers"
	                System.err.println("OTP not sent: Twilio trial accounts cannot send messages to unverified numbers.");
	            } else {
	                System.err.println("Error sending OTP via SMS: " + e.getMessage());
	            }
	        }
	    }
	
	public Employee updateEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}

	public void deleteEmployeeById(int employeeId) {
		employeeRepository.deleteById(employeeId);
	}
}