package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.dao.CustomerDao;
import com.model.Customer;

@RestController
public class CustomerController {
	
	@Autowired
	CustomerDao customerDao;
	
	@GetMapping("getAllCustomers")
	public List<Customer> getAllCustomers(){
		return customerDao.getAllCustomers();
	}
	
	@PostMapping("addCustomer")
	public Customer addProduct(@RequestBody Customer customer) {
		return customerDao.addCustomer(customer);
	}
	
	@GetMapping("customerLogin/{emailId}/{password}")
	public Customer employeeLogin(@PathVariable String emailId, @PathVariable String password) {
		return customerDao.customerLogin(emailId, password);
	}

}
