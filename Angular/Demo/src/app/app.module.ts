import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { Test2Component } from './test2/test2.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';


import { ProductsComponent } from './products/products.component';
import { ExpPipe } from './exp.pipe';
import { ShowEmployeeComponent } from './show-employee/show-employee.component';
import { GenderPipe } from './gender.pipe';
import { ToastrModule } from 'ngx-toastr';
import { HeaderComponent } from './header/header.component';
import { LogoutComponent } from './logout/logout.component';
import { RouterModule } from '@angular/router';
import { ShowempbyidComponent } from './showempbyid/showempbyid.component';
import { HttpClientModule } from '@angular/common/http';
import { CartComponent } from './cart/cart.component';
import { NgxCaptchaModule } from 'ngx-captcha';



@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    Test2Component,
    LoginComponent,
    RegisterComponent,
    
    
    ProductsComponent,
    ExpPipe,
    ShowEmployeeComponent,
    GenderPipe,
    HeaderComponent,
    LogoutComponent,
    ShowempbyidComponent,
    CartComponent,

 
    
    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,

    ToastrModule.forRoot(),
    RouterModule,
    ReactiveFormsModule,
    NgxCaptchaModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
